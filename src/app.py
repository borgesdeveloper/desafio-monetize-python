from flask import Flask, render_template
from core import result

app = Flask(__name__, template_folder= 'templates')

@app.route('/')
def index():

    result_game = result.Result()
    result_game.set_quantidade_de_jogos(10)
    result_game.set_dezenas(6)
    result_game.gerar()
    result_game.sortear()
    result_game.formatar_resultado()
    return render_template('index.html', result = result_game)

if __name__ == '__main__':
    app.run(port=9999)