import random
import sys
import random
from infra import generator

class Result():

    quantidade_de_jogos = 2
    quantidade_de_dezenas = 1
    resultado = []
    resultado_formatado = ""
    jogos = []

    def set_jogos(self, p_jogos):
        self.jogos = p_jogos

    def get_jogos(self):
        return self.jogos


    def set_result(self, p_resultado = []):
        self.resultado = p_resultado

    def get_result(self):
        return self.resultado

    def get_dezenas(self):
        return self.resultado

    def set_dezenas(self, p_dezenas):
        self.quantidade_de_dezenas = p_dezenas

    def get_quantidade_de_jogos(self):
        return self.quantidade_de_jogos

    def set_quantidade_de_jogos(self, p_quantidade_de_jogos):
        self.quantidade_de_jogos = p_quantidade_de_jogos

    def sortear(self):
        
        result_numbers = generator.gerar_dezenas(self.quantidade_de_dezenas)

        for jogo in self.jogos:
            count = 0
            
            for result_number in result_numbers:
                if result_number in jogo['numeros']:
                    count +=1
                    
            jogo['quantidade_de_acertos'] = count
    
        self.set_result(result_numbers)

    def formatar_resultado(self):
        
        for result in self.resultado:
            self.resultado_formatado += f'({result})' 

        for jogo in self.jogos:
            for numero in jogo['numeros']:
                jogo['numeros_apostados'] += f'({numero})'

    def gerar(self):

        results = []
        
        for i in list(range(self.quantidade_de_jogos)):
            result = generator.gerar_dezenas(self.quantidade_de_dezenas)

            results.append({
                "jogo": i,
                "quantidade_de_acertos":0,
                "numeros_apostados":"",
                "numeros": result
            })
                
        self.set_jogos(results)


