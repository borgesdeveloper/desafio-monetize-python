import random
import sys
import random

def gerar_dezenas(self, qty = 6):

        numbers = list(range(1, 61))
        random.shuffle(numbers)
        selected_numbers = []
        even_numbers = 0
        odd_numbers = 0

        for num in numbers:
            if len(selected_numbers) == qty:
                break

            if num in [26, 55, 60, 40, 22, 39, 21, 57, 19, 25]:
                continue

            if num > 9:
                has_similar = False
                for sel in selected_numbers:
                    if str(num)[-1] == str(sel)[-1]:
                        has_similar = True
                        break
                if has_similar:
                    continue

            if (num + 1) in selected_numbers or (num - 1) in selected_numbers:
                continue

            if num % 2 and odd_numbers > even_numbers:
                continue

            if not num % 2 and even_numbers > odd_numbers:
                continue

            selected_numbers.append(num)

            if num % 2:
                odd_numbers += 1
            else:
                even_numbers += 1

        selected_numbers.sort()
        selected_numbers = [str(num).rjust(2, '0') for num in selected_numbers]

        number_generated = sorted(selected_numbers)
        return number_generated